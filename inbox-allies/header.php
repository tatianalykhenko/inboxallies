<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Inbox_Allies
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<div class="brand text-center text-lg-left">
						<?php the_custom_logo(); ?>
					</div>
					<div class="toggle-btn">
			            <span class="hamburger">
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			            </span>
			       	</div>
				</div>
				<div class="col-lg-9">
					<div class="main-menu">
						<div class="main-nav">
						    <div class="menu-primary-menu-container">
						    	<?php
									wp_nav_menu(
										array(
											'theme_location' => 'menu-1',
											'menu_id'        => 'primary-menu',
										)
									);
								?>
							</div>						
						</div><!-- #main-nav -->
					</div>
					
				</div>
			</div>
		</div>
	</header>