<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Inbox_Allies
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section id="pricing-hero">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8 offset-lg-2 text-center">
					<h1><?php the_field('page_title'); ?></h1>
					<p>
						<?php the_field('page_subtitle'); ?>
					</p>
				</div>
			</div>
		</div>
	</section>
	<section id="pricing">
		<div class="container">
			<div class="row">
				<?php

					if( have_rows('prices') ):

					    while( have_rows('prices') ) : the_row(); ?>

					    	<div class="col-lg-4 mb-4">
								<div class="pricecard text-center">
									<div class="price-name">
										<?php the_sub_field('name'); ?>
									</div>
									<div class="price-description">
										<?php the_sub_field('description'); ?>
									</div>
									<div class="price-value">
										<?php the_sub_field('amount'); ?>
									</div>
									<div class="price-note">
										<?php the_sub_field('note'); ?>
									</div>
									<div class="price-button">
										<a href="<?php the_sub_field('button_url'); ?>" class="btn transparent-button"><?php the_sub_field('button_text'); ?></a>
									</div>
								</div>
							</div>

					    <?php endwhile;

					endif;
				?>

			</div>
		</div>
	</section>
	<section id="trusted-companies">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center mb-5">
					<h2><?php the_field('trusted_companies_title'); ?></h2>
				</div>
			</div>
			<div class="row">
				<div class="trusted-companies-logo">
					<?php

						if( have_rows('trusted_companies') ):

						    while( have_rows('trusted_companies') ) : the_row(); ?>

						    	<?php $image = get_sub_field('company_logo'); ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">

						    <?php endwhile;

						endif;
					?>
				</div>
			</div>
		</div>
	</section>
</article><!-- #post-<?php the_ID(); ?> -->
