<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Inbox_Allies
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="entry-header">
					<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

					<?php if ( 'post' === get_post_type() ) : ?>
					<div class="entry-meta">
						<?php
						inbox_allies_posted_on();
						inbox_allies_posted_by();
						?>
					</div><!-- .entry-meta -->
					<?php endif; ?>
				</div><!-- .entry-header -->

				<?php inbox_allies_post_thumbnail(); ?>

				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div><!-- .entry-summary -->

				<div class="entry-footer">
					<?php inbox_allies_entry_footer(); ?>
				</div><!-- .entry-footer -->
			</div>
		</div>
	</div>
				
</article><!-- #post-<?php the_ID(); ?> -->
