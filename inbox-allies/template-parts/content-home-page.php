<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Inbox_Allies
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section id="home-hero">
		<div class="container">
			<div class="row h-100">
				<div class="col-lg-7 my-auto">
					<div class="section-label">
						<?php the_field('section_label'); ?>
					</div>
					<h1>
						<?php the_field('page_title'); ?>
					</h1>
					<p class="mb-5">
						<?php the_field('page_subtitle'); ?>
					</p>
					<a href="<?php the_field('hero_button_url'); ?>" class="btn blue-button mb-5 mb-lg-0">
						<?php the_field('hero_button_text'); ?>
					</a>
				</div>
				<div class="col-lg-5 my-auto text-center">
					<?php $image = get_field('hero_image'); ?>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid email-marketing">
				</div>
			</div>
		</div>
	</section>
	<section id="our-services">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center mb-5">
					<h2><?php the_field('our_services_title'); ?></h2>
				</div>
			</div>
			<div class="row mb-5">
				<?php

					if( have_rows('services') ):

					    while( have_rows('services') ) : the_row(); ?>

					    	<div class="col-lg-4">
								<div class="service-wrap">
									<div class="service-icon">
										<?php $image = get_sub_field('service_icon'); ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php the_sub_field('service_title'); ?>">
									</div>
									<div class="service-title text-center">
										<h4><?php the_sub_field('service_title'); ?></h4>
									</div>
									<div class="service-description text-center">
										<?php the_sub_field('service_description'); ?>
									</div>
								</div>
							</div>
					        

					    <?php endwhile;

					endif;
				?>
			</div>
			<div class="row">
				<div class="col-12 text-center">
					<a href="<?php the_field('our_services_button_url'); ?>" class="btn transparent-button"><?php the_field('our_services_button_text'); ?></a>
				</div>
			</div>
		</div>
	</section>
	<section id="our-cases">
		<div class="container-fluid position-relative px-lg-0">
			<div class="case-slider">
				<?php

					if( have_rows('cases') ):

					    while( have_rows('cases') ) : the_row(); ?>

					    	<div class="slide-item">
								<div class="row">
									<div class="col-lg-6 pr-xl-5 d-flex">
										<div class="section-label">
											<?php the_sub_field('case_label'); ?>
										</div>
										<div class="case-data my-auto">
											<h3 class="blue mb-3 mb-lg-5">
												<?php the_sub_field('case_title'); ?>
											</h3>
											<p class="mb-3 mb-lg-5">
												<?php the_sub_field('case_description'); ?> 
											</p>
											<a href="<?php the_sub_field('case_button_url'); ?>" class="btn transparent-button"><?php the_sub_field('case_button_text'); ?></a>
										</div>
									</div>
									<div class="col-lg-6">
										<?php $image = get_sub_field('case_image'); ?>
										<div class="case-image" style="background-image: url(<?php echo $image['url']; ?>);"></div>
									</div>
								</div>
							</div>

					    <?php endwhile;

					endif;
				?>
			</div>
			<div class="slide-controls mt-4 mt-lg-0">
				<div class="prev-slide"></div>
				<div class="next-slide"></div>
			</div>	
		</div>
	</section>
	<section id="testimonials">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h2 class="mb-5"><?php the_field('testimonials_title'); ?></h2>
				</div>
			</div>
			<div class="row accordion" id="testitems">
				<?php if (get_field('testimonials')): ?>
				<div class="col-lg-6 col-xl-5 mb-4">
					<?php $counter = count(get_field('testimonials'));?>
					<?php $i = 1; ?>
					<?php

						if( have_rows('testimonials') ):

						    while( have_rows('testimonials') ) : the_row(); ?>


						    	<div class="collapse-button <?php if ($i != 1) echo 'collapsed'?>" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="<?php if ($i != 1) echo 'false'; else echo 'true'; ?>" aria-controls="collapse<?php echo $i; ?>">
								    <div class="row h-100">
								    	<div class="col-4 col-lg-3 my-auto">
								    		<div class="testiavatar" style="background-image: url(<?php the_sub_field('avatar'); ?>);"></div>
								    	</div>
								    	<div class="col-8 col-lg-9 my-auto">
								    		<h5 class="mb-0"><?php the_sub_field('name'); ?></h5>
								    		<div class="testiposition"><?php the_sub_field('position'); ?></div>
								    	</div>
								    </div>
								</div>
								<?php $i++; ?>
						    <?php endwhile;

						endif;
					?>
				</div>
				<div class="col-lg-6 offset-xl-1 testilist">
					<?php $i = 1; ?>
					<?php

						if( have_rows('testimonials') ):

						    while( have_rows('testimonials') ) : the_row(); ?>

						    	<div id="collapse<?php echo $i; ?>" class="collapse <?php if ($i == 1) echo 'show'?>" aria-labelledby="heading<?php echo $i; ?>" data-parent="#testitems">
									<h4 class="mb-3"><?php the_sub_field('title'); ?></h4>
									<div class="testirating">
										<?php $rating = get_sub_field('rating'); ?>
										<?php for ($q = 1; $q <= 5; $q++) {
										    if ($q <= $rating){
											    echo '<span class="mr-1 checked"></span>';
											} else {
												echo '<span class="mr-1"></span>';
											}
										} ?>
									</div>
							        <?php the_sub_field('feedback'); ?>
							    </div>


								<?php $i++; ?>
						    <?php endwhile;

						endif;
					?>

				</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
</article><!-- #post-<?php the_ID(); ?> -->
