<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Inbox_Allies
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section id="contact-hero">
		<div class="container">
			<div class="row h-100">
				<div class="col-lg-7 col-xl-6 my-auto">
					<h1><?php the_field('page_title'); ?></h1>
					<?php the_field('page_descriptions'); ?>
				</div>
				<div class="col-lg-5 col-xl-4 offset-xl-2">
					<div class="contact-form-wrap mt-3 mt-lg-5">
						<?php $form = get_field('form_code'); ?>
						<?php echo do_shortcode($form);?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="trusted-companies">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center mb-5">
					<h2><?php the_field('trusted_companies_title', '11'); ?></h2>
				</div>
			</div>
			<div class="row">
				<div class="trusted-companies-logo">
					<?php

						if( have_rows('trusted_companies', '11') ):

						    while( have_rows('trusted_companies', '11') ) : the_row(); ?>

						    	<?php $image = get_sub_field('company_logo'); ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">

						    <?php endwhile;

						endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<section id="testimonials">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h2 class="mb-5"><?php the_field('testimonials_title', '16'); ?></h2>
				</div>
			</div>
			<div class="row accordion" id="testitems">
				<?php if (get_field('testimonials', '16')): ?>
				<div class="col-lg-6 col-xl-5 mb-4">
					<?php $counter = count(get_field('testimonials', '16'));?>
					<?php $i = 1; ?>
					<?php

						if( have_rows('testimonials', '16') ):

						    while( have_rows('testimonials', '16') ) : the_row(); ?>


						    	<div class="collapse-button <?php if ($i != 1) echo 'collapsed'?>" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="<?php if ($i != 1) echo 'false'; else echo 'true'; ?>" aria-controls="collapse<?php echo $i; ?>">
								    <div class="row h-100">
								    	<div class="col-4 col-lg-3 my-auto">
								    		<div class="testiavatar" style="background-image: url(<?php the_sub_field('avatar'); ?>);"></div>
								    	</div>
								    	<div class="col-8 col-lg-9 my-auto">
								    		<h5 class="mb-0"><?php the_sub_field('name'); ?></h5>
								    		<div class="testiposition"><?php the_sub_field('position'); ?></div>
								    	</div>
								    </div>
								</div>
								<?php $i++; ?>
						    <?php endwhile;

						endif;
					?>
				</div>
				<div class="col-lg-6 offset-xl-1 testilist">
					<?php $i = 1; ?>
					<?php

						if( have_rows('testimonials', '16') ):

						    while( have_rows('testimonials', '16') ) : the_row(); ?>

						    	<div id="collapse<?php echo $i; ?>" class="collapse <?php if ($i == 1) echo 'show'?>" aria-labelledby="heading<?php echo $i; ?>" data-parent="#testitems">
									<h4 class="mb-3"><?php the_sub_field('title'); ?></h4>
									<div class="testirating">
										<?php $rating = get_sub_field('rating'); ?>
										<?php for ($q = 1; $q <= 5; $q++) {
										    if ($q <= $rating){
											    echo '<span class="mr-1 checked"></span>';
											} else {
												echo '<span class="mr-1"></span>';
											}
										} ?>
									</div>
							        <?php the_sub_field('feedback'); ?>
							    </div>


								<?php $i++; ?>
						    <?php endwhile;

						endif;
					?>

				</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
</article><!-- #post-<?php the_ID(); ?> -->
