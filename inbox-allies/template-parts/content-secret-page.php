<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Inbox_Allies
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section id="secret-hero">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8 offset-lg-2 text-center">
					<h1><?php the_field('page_title'); ?></h1>
					<p>
						<?php the_field('page_subtitle'); ?> 
					</p>
				</div>
			</div>
		</div>
	</section>

	<?php

		if( have_rows('secret_sauces') ):

			$counter = count(get_field('secret_sauces'));
			$i = 1;

		    while( have_rows('secret_sauces') ) : the_row(); ?>


		    	<section class="secret-sauce <?php if ($i == $counter) echo 'mb-5'; ?>">
					<div class="container-fluid">
						<div class="row h-100 no-gutters">
							<div class="col-lg-6 <?php if($i % 2 === 0) echo 'pl-lg-4 pl-xl-5 order-lg-2'; else echo 'pr-lg-4 pr-xl-5'; ?> d-flex">
								<div class="section-label">
									<?php the_sub_field('sauce_label'); ?>
								</div>
								<div class="sauce-data my-auto">
									<h3 class="blue mb-3 mb-lg-5">
										<?php the_sub_field('sauce_title'); ?>
									</h3>
									<p class="mb-3 mb-lg-5">
										<?php the_sub_field('sauce_description'); ?>
									</p>
									<a href="<?php the_sub_field('sauce_button_url'); ?>" class="btn transparent-button"><?php the_sub_field('sauce_button_text'); ?></a>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="sauce-image" style="background-image: url(<?php the_sub_field('sauce_image'); ?>);"></div>
							</div>
						</div>
					</div>
				</section>

		    <?php

		    	$i++;

				endwhile;

		endif;
	?>

</article><!-- #post-<?php the_ID(); ?> -->
