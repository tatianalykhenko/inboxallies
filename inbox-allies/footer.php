<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Inbox_Allies
 */

?>
	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h2 class="mb-4">
						<?php the_field('footer_title', 'options'); ?>
					</h2>
					<p class="mb-5">
						<?php the_field('footer_subtitle', 'options'); ?>
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="contacts-wrap">
						<div class="footer-email">
							Email us:<br>
							<a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a>
						</div>
						<div class="footer-phone">
							<?php
							    $phone_number = get_field('phone', 'option');
							    $phone_number = preg_replace('/[^0-9]/', '', $phone_number);
							?>
							Call us:<br>
							<a href="tel:<?php echo $phone_number; ?>"><?php the_field('phone', 'options'); ?></a>
						</div>
						<div class="footer-chat">
							<a href="<?php the_field('footer_button_url', 'options'); ?>" class="btn blue-button"><?php the_field('footer_button_text', 'options'); ?></a>
						</div>
					</div>
					<div class="hr"></div>
				</div>
			</div>
			<div class="row h-100 mb-4">
				<div class="col-lg-3 my-auto">
					<div class="brand text-center text-lg-left mb-4 mb-lg-0">
						<?php the_custom_logo(); ?>
					</div>
				</div>
				<div class="col-lg-9">
					<div class="footer-menu">
						<div class="footer-nav">
						    <div class="menu-footer-menu-container">
						    	<?php
									wp_nav_menu(
										array(
											'theme_location' => 'menu-2',
											'menu_id'        => 'footer-menu',
										)
									);
								?>

							</div>						
						</div>
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 text-center text-lg-left mb-3 mb-lg-0">
					<a href="<?php the_field('privacy_policy', 'options'); ?>" class="privacy-policy">Privacy Policy</a>
				</div>
				<div class="col-lg-6 text-center text-lg-right">
					<div class="copyright">
						<?php the_field('copyrights', 'options'); ?>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
